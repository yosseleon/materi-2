package main

import (
	"fmt"
)

func main() {

	const conferenceTickets int = 50
	var remainingTickets uint = 50
	conferenceName := "Go Conference"


	fmt.Printf("Welcome to %v booking application.\nWe have total of %v tickets and %v are still available.\nGet your tickets here to attend\n", conferenceName, conferenceTickets, remainingTickets)

	city := "London"

	switch city {
	case "New York":
	case "hongkong":
	case "Berlin":
	case "Singapore":
	case "London":
	case "Mexico City":
	default:
		fmt.Print("No valid city selected")
	}
