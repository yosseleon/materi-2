// console.log('Hello world');

// let age = 30;
// age = 31;

// console.log(age);

// String, numbers, boolean, null, undifined
// const name = 'JOhn';
// const age = 30;
// const rating = .5;
// const isCool = true;
// const x = null;
// const y = undefined;
// let z;

// console.log(typeof x);

// const name = 'john';
// const age = 30;
//  concatentation
// console.log('my name is ' + name + 'and my age' + age);
// template string
// console.log('My name is ${name} and my age ${age}');
// console.log(hello);

// const s= 'hello world'

// console.log(s.substring(0,5).toUpperCase());
// console.log(s.split(''));

// array - variable that hold multiple values

// const number = new Array(1,2,3,4)

// console.log(number);

// const fruits =['apple','oranges','pears']

// fruits[3]='grapes';

// fruits.push('mangos');
// fruits.unshift('strawberry');
// fruits.pop();
// console.log(fruits);

// const person = {
    // firstName: 'john',
    // lastName: 'Doe',
    // age: 30,
    // hobbies: ['music', 'movies', 'sports'],
    //    address: {
    //    street: '50 st main',
    //    city: 'Boston',
    //    state: 'MA'
    //  }
    // }

// person.email = 'john@gmail.com';

// console.Console.log(person);

// const todos = [{
            //  id: 1,
            //  text: 'Take out trash',
            //  isCompleted: true
    
        //  },
        //  {
            //  id: 2,
            //  text: 'Meeting with boss',
            //  isCompleted: true
    
        //  },
        //  {
            //  id: 3,
            //  text: 'Dentist appt',
            //  isCompleted: false
    
        //  },
    //  ];

// console.log(todos);
// console.log(todos[1].text);

// const todoJSON = JSON.stringify(todos);
// console.log(todoJSON);

// for (let i = 0; i < todos.length; i++) {
//     console.log(`for loop number: ${i}`);
//     console.log(todos[i].text);
//  }

// for (let todo of todos) {
    //     console.log(todo.text);
    // }
    
    //foreach, map, filter
    // todos.forEach(function(todo) {
    //     console.log(todo.text);
    // });
    
    // const todoText = todos.map(function(todo) {
    //     return todo.text;
    // });

    //  console.log(todoText);

// const todoCompleted = todos.filter(function(todo) {
//     return todo.isCompleted === true;
// }).map(function(todo) {
//     return todo.text;
// })

//if else
// const x = 6;
// const y = 9;

// if (x === 10) {
//     console.log('x is 10');
// } else if (x > 10) {
//     console.log('x is greater than 10');
// } else {
//     console.log('x is less than 10');
// }

// if (x > 5 || y > 10) {
//     console.log('x is more than 5 or y is more than 10');
// }

// if (x > 5) {
//     if (y > 10) {
//         console.log('x is more than 5 and y is more than 10');
//     }
// }

// const x = 10;
// const color = x > 10 ? 'red' : 'blue';

// console.log(color);
// const color = 'green'
// switch (color) {
//     case 'red':
//         console.log('color is red');
//         break;
//     case 'blue':
//         console.log('color is blue');
//         break;

//     default:
//         console.log('color is NOT red or blue');
//         break;
// }

// function addNums(num1, num2) {
//     console.log(num1 + num2);
// }
    
// addNums(2, 4);
    
// const addNums = (num1 = 1, num2 = 1) => {
//     console.log(num1 + num2);
// }
    
// const addNums = (num1 = 1, num2 = 1) => num1 + num2;
    
// console.log(addNums(5, 5));
    
// constructor function
// function Person(firstName, lastName, dob) {
//     this.firstName = firstName;
//     this.lastName = lastName;
//     this.dob = new Date(dob);
// }
    
// Person.prototype.getBirthYear = function() {
//     return this.dob.getFullYear();
// }
    
// Person.prototype.getFullName = function() {
//     return `${this.firstName} ${this.lastName}`;
// }
    
// // class
// class Person {
//     constructor(firstName, lastName, dob) {
//         this.firstName = firstName;
//         this.lastName = lastName;
//         this.dob = new Date(dob);
//     }
    
//     getBirthYear() {
//         return this.dob.getFullYear();
//     }
    
//     getFullName() {
//         return `${this.firstName} ${this.lastName}`;
//     }
// }


// instantiate object
// const person1 = new Person('john', 'doe', '4-3-1980');
// const person2 = new Person('marry', 'smith', '4-6-1980');

// // console.log(person1);
// console.log(person1.getFullName());
// console.log(person1.getBirthYear());

// single element
// console.log(document.getElementById('my-form'));
// console.log(document.querySelector('.container'));

// multiple element
// console.log(document.querySelectorAll('.item'));
// console.log(document.getElementsByTagName('li'));
// console.log(document.getElementsByClassName('item'));
// console.log(document.getElementsByTagName('li'));

// const items = document.querySelectorAll(`.item`);

// items.forEach((item) => console.log(item));

// const ul = document.querySelector(`.items`);

// // ul.remove()
// // ul.lastElementChild.remove();
// ul.firstElementChild.textContent = 'Hello';
// ul.children[1].innerText = 'Brad';
// ul.lastElementChild.innerHTML = '<h1>Hello</h1>'

// const btn = document.querySelector(`.btn`);

// btn.style.background = 'red';
// btn.addEventListener('click', (e) => {
//     e.preventDefault();
//     console.log(e.target.className);
//     document.getElementById('my-form').style.background = '#ccc';
//     document.querySelector('body').classList.add('bg-dark');
//     ul.lastElementChild.innerHTML = '<h1>Changed</h1>';
// });

const myForm = document.querySelector('#my-form');
const nameInput = document.querySelector('#name');
const emailInput = document.querySelector('#email');
const msg = document.querySelector('.msg');
const userList = document.querySelector('#users');

myForm.addEventListener('submit', onSubmit);

function onSubmit(e) {
    e.preventDefault();

    // console.log(nameInput.value);

    if (nameInput.value === '' || emailInput.value === '') {
        alert('Please enter all fields');
        msg.classList.add('error');
        msg.innerHTML = 'Please enter all fields';

        // Remove error after 3 seconds
        setTimeout(() => msg.remove(), 3000);
    } else {
        // Create new list item with user
        const li = document.createElement('li');

        // Add text node with input values
        li.appendChild(document.createTextNode(`${nameInput.value}: ${emailInput.value}`));

        // Add HTML
        // li.innerHTML = `<strong>${nameInput.value}</strong>e: ${emailInput.value}`;

        // Append to ul
        userList.appendChild(li);

        // Clear fields
        nameInput.value = '';
        emailInput.value = '';
    }
}